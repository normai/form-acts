<img src="./docs/20230927o1853.utenti-architetto-france-01.v2.x0128y0128.png" align="right" width="96" height="96" alt="Form-Acts Logo">

# Form-Acts Demo &nbsp; <sup><sub><sup>v0.0.6 <del>v0.0.4</del></sup></sub></sup>

This is a small **PHP/MySQL website demonstrating Login, PDO, AJAX, MVC**,
 means all types of communication between HTML-page and webserver and DOM.

<cite>Todo: Make it database agnostic. Switch ahead a form, telling
 which database connections are available, and give the user the controls
 to configure the database.</cite>

## Project Synopsis

- Slogan : Demo HTML form handling with PHP and JavaScript
- Status: It works proof-of-concept, much grinding and refactoring is wanted
- Authors : Norbert C. Maier, Dave Hollingworth, W.S.Toh
- License : BSD-3-Clause

## Details

- HTML Form elements
- Send and process form data via plain PHP
- Send and process form data via classic XmlHttpRequest
- Send and process form data via HTML5 Fetch-API
- Access MySql database via PHP/PDO
- MVC design pattern
- User registration
- User login

## Requirements

To run this demo, the following requirements have to be met:
- The files must reside in a folder served by a PHP-capable server
- The connection to a MySQL server must be available
- On the MySQL server, a database 'form-acts' must exist

## References

This demo grew from the following two projects:

<img src="./docs/20201203o1555.codeboxx.v1.x0048y0048.png" align="center" width="48" height="48" alt="Link icon CodeBoxx">
 CodeBoxx article
 [PHP MVC With Database (Very Simple Beginner Example](https://code-boxx.com/simple-php-mvc-example/)
 by W.S.Toh with the code on GitHub gist
 [code-boxx/0-PHP-MVC.MD](https://gist.github.com/code-boxx/372bb08f4763937aed08b678ca900781)

&nbsp;

<img src="./docs/20230923o1555.davehollingworth.v1.x0048y0048.png" align="center" width="48" height="48" alt="Link icon DaveHollingworth">
 GitHub project
 [Signup and Login with PHP and MySQL](https://github.com/daveh/php-signup-login)
 by Dave Hollingworth with the accompanying
 [YouTube video](https://www.youtube.com/watch?v=5L9UhOnuos0)
 (43:15 Min)

&nbsp;

The top right project logo comes from:

<img src="./docs/20201113o0203.openclipart.v1.x0048y0048.png" align="center" width="48" height="48" alt="Link icon OpenClipArt">
 OpenClipArt page
 [Architetto -- utenti](https://openclipart.org/detail/34963/architetto-utenti)
 by Francesco 'Architetto' Rollandin

&nbsp;

## Changelog

 log 20241128°0751 v0.0.6 — Few difference to v0.0.5

 log 2024xxxx°xxxx v0.0.5 — ~Fine tune

 log 20230926°1711 v0.0.1 — Project createion

&nbsp;

<sup><sub>*[project 20230926°1711, file 20230926°1721]* ⬞Ω</sub></sup>
