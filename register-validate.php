<?php
// file : 20230926°2141 register-validate.php
// origin : Spawned from 20230926°2121 daveh-process-signup.php
// status : Not active yet, must be converted to use the PDO-API

$mysqli = require __DIR__ . "/daveh-database.php";

$sql = sprintf( "SELECT * FROM users WHERE email = '%s'"
               , $mysqli->real_escape_string($_GET["email"])
                );

$result = $mysqli->query($sql);

$is_available = $result->num_rows === 0;

header("Content-Type: application/json");

echo json_encode(["available" => $is_available]);
