﻿<?php
/**
 * file        : 20230926°1851 model.php
 * origin      : 1-model.php from https://gist.github.com/code-boxx/372bb08f4763937aed08b678ca900781 [ref 20230923°1423]
 * callers     : 20230926°1751 controller.php, ...
 */

class DB {
   // (A) Connect to database [seq 20230926°1852]
   public $error = "";
   ////private $pdo = null;
   public $pdao = null;          // PROVISORY PUBLIC, UNTIL SEQ 20230927°1731 IS INTEGRATED HERE
   private $stmt = null;

   function __construct () {

      try {
         $this->pdao = new PDO ( "mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET
                                , DB_USER, DB_PASSWORD
                                 , [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]
                                  );
      }
      catch(PDOException $e) {
         die('Verbindung zu ' . DB_HOST . '/' . DB_NAME . '<br>' . $e->getMessage());
      }
      if ($this->pdao === null) {
         die("Connecting to database failed.");
      }
   }

   // (B) Close connection [seq 20230926°1853]
   function __destruct () {
      if ($this->stmt !== null) { $this->stmt = null; }
      if ($this->pdao !== null) { $this->pdao = null; }
   }


   // () Guarantee tables [func 20230927°1821]
   // ref : https://glasshost.tech/check-if-a-database-table-exists-using-php-pdo/ [ref 20230927°1832]
   // ref : https://stackoverflow.com/questions/1525784/mysql-check-if-a-table-exists-without-throwing-an-exception [ref 20230927°1836]
   // ref : https://stackoverflow.com/questions/1717495/check-if-a-database-table-exists-using-php-pdo [ref 20230927°1837]
   function initialize_tables() {
      //die('Aloha');


      // () [seq 20230927°1941]
      // ref : https://fedingo.com/how-to-check-if-mysql-database-exists/ [ref 20230927°1932]
      // ref : https://www.mysqltutorial.org/mysql-create-database/ [ref 20230927°1952]
      $dbName = DB_NAME; // 'form_acts'; //// 'form_acts'; //// DB_NAME; //// 'DB_NAME';

// Sequence provisory shutdown due to issue [log 20230927°2021]
// See issue 20230927°2021 'No PDO without database'
if (False) {
      $sql = "CREATE DATABASE IF NOT EXISTS :database;";
      ////$sql = "CREATE DATABASE IF NOT EXISTS form_acts;";
      $stmt = $this->pdao->prepare($sql);
      $stmt->bindParam(':database', $dbName);
      try {
         $stmt->execute();
      }
      catch(PDOException $e) {
         die($sql . '<br>' . $e->getMessage());
      }
}


      // [seq 20230927°1822]
      $tableName = 'names';
      $sql = "SELECT COUNT(*) FROM information_schema.tables"
            . " WHERE table_schema = :database AND table_name = :table"
             ;
      $stmt = $this->pdao->prepare($sql);
      $stmt->bindParam(':database', $dbName);
      $stmt->bindParam(':table', $tableName);
      $stmt->execute();
      $tableExists = $stmt->fetchColumn() > 0 ? True : False;

      //die('DEBUG DB ' . $dbName . ', Table ' . $tableName . ' ' . ($tableExists ? 'existiert' : 'fehlt'));

      // () [seq 20230927°1823]
      // ref : https://www.w3schools.com/php/php_mysql_create_table.asp [ref 20230927°1842]
      if (! $tableExists) {

         // [seq 20230927°1824]
         try {
            // () Create table
            $sql = "CREATE TABLE names "
                 . "( ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY"
                 . ", Forename VARCHAR(30) NOT NULL"
                 . ", Surname VARCHAR(30) NOT NULL"
                 . ", reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
                 . ")"
                 ;

            // use exec() because no results are returned
            $this->pdao->exec($sql);
            ////echo("Table MyGuests created successfully");
         }
         catch(PDOException $e) {
            die($sql . '<br>' . $e->getMessage());
         }


         // () [seq 20230927°1825]
         // ref : https://www.w3schools.com/php/php_mysql_insert.asp [ref 20230927°1843]
         // ref : https://stackoverflow.com/questions/9744192/multi-line-string-literals-in-php [ref 20230927°1844]
         try {
            ////$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            ////// set the PDO error mode to exception
            ////$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $sql = <<<ULANBATOR
               INSERT INTO names (Forename, Surname) VALUES
                    ('Abby', 'Ward')         , ('Aleksandra', 'Devine') , ('Aston', 'Simmonds')
                  , ('Beth', 'Skywalker')    , ('Bridget', 'Wooten')    , ('Coby', 'Kelleigh')
                  , ('Darrah', 'Shadow')     , ('Drake', 'Adelaide')    , ('Elizabeth', 'Stewart')
                  , ('Gareth', 'Solderini')  ,('Gregor', 'Bryant')      , ('Gregor', 'Bryant')
                  , ('Hannah', 'Strickland') ,('Harvey', 'Frame')       , ('Imogene', 'Thad')
                  , ('Ismaeel', 'Carty')     ,('Issac', 'Calderon')     , ('Jane', 'Doe')
                  , ('Jaslyn', 'Keely')      ,('Jax', 'Howe')           , ('Joey', 'Whyte')
                  , ('John', 'Doe')          ,('Joseph', 'Stewart')     , ('Julia', 'Greaves')
                  , ('Junior', 'Douglas')    ,('Kaiden', 'Bentley')     , ('Kenneth', 'Sanders')
                  , ('Keziah', 'Knapp')      ,('Kirstie', 'Thomas')     , ('Lawrence', 'Murphy')
                  , ('Leah', 'Shan')         ,('Marcus', 'Best')        , ('Maya', 'Paine')
                  , ('Myla', 'Bostock')      ,('Nathaniel', 'Khan')     , ('Peers', 'Sera')
                  , ('Richard', 'Breann')    ,('Rowan', 'Avalos')       , ('Rusty', 'Terry')
                  , ('Sacha', 'Gross')       ,('Sally', 'Castillo')     , ('Sarah', 'Sanders')
                  , ('Seth', 'Sonnel')       ,('Shannon', 'Peterson')   , ('Shayan', 'Clements')
                  , ('Shoaib', 'Vickers')    ,('Sulaiman', 'Gilmour')   , ('Taran', 'Morin')
                  , ('Thelma', 'Kim')        ,('Tillie', 'Sharalyn')    , ('Virgil', 'Collier')
            ULANBATOR;

            // Use exec() because no results are returned
            $this->pdao->exec($sql);
            ////echo("New record created successfully");
         }
         catch(PDOException $e) {
            ////echo($sql . "<br>" . $e->getMessage());
            die($sql . "<br>" . $e->getMessage());
         }
      }


      // () [seq 20230928°1711]
      $tableName = 'users';
      $sql = "SELECT COUNT(*) FROM information_schema.tables"
            . " WHERE table_schema = :database AND table_name = :table"
             ;
      $stmt = $this->pdao->prepare($sql);
      $stmt->bindParam(':database', $dbName);
      $stmt->bindParam(':table', $tableName);
      $stmt->execute();
      $tableExists = $stmt->fetchColumn() > 0 ? True : False;

      //die('DEBUG Table users: ' . $tableExists ? 'Existiert' : 'Fehlt');

      // () [seq 20230928°1721]
      if (! $tableExists) {

         // [seq 20230928°1731]
         try {
            // () Create table [seq 20230928°1733]
            $sql = "CREATE TABLE users "
                 . "( ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY"
                 . ", Name VARCHAR(30) NOT NULL"
                 . ", Email VARCHAR(30) NOT NULL"
                 . ", Pw_Hash VARCHAR(80) NOT NULL"
                 . ", Timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
                 . ")"
                 ;
            $this->pdao->exec($sql);
         }
         catch(PDOException $e) {
            die($sql . '<br>' . $e->getMessage());
         }

         // () [seq 20230928°1741]
         try {

            // [seq 20230928°1743]
            // ref : https://www.momjunction.com/ [ref 20230927°2012]
            // Pws: Ada45678, Bea45678
            $sql = <<<ULANBATOR
               INSERT INTO users (Name, Email, Pw_Hash) VALUES
                    ('Ada', 'ada@x.lum', '$2y$10$6P5LGNT5FC9yXB5fp5ZqmePgonHnRmq8b5tC6X6clofRCKubALGPq')
                  , ('Bea', 'bea@x.lum', '$2y$10$PcaoC7IV4WfwxthdQXktAOynaj5HTGlEDUL.CxtFX43VXig5.ZAc2')
            ULANBATOR;

            $this->pdao->exec($sql);
         }
         catch(PDOException $e) {
            die($sql . "<br>" . $e->getMessage());
         }
      }
   }


   // (C) Run SELECT query [seq 20230926°1854]
   function select ($sql, $data = null) {
      $this->stmt = $this->pdao->prepare($sql);
      $this->stmt->execute($data);
      $x = $this->stmt->fetchAll();
      return $x;
   }
}


// (D) Database settings — Adjust this as needed [seq 20230926°1855]
define("DB_HOST"    , "localhost");
define("DB_NAME"    , 'form_acts'); //// 'form_acts'); /// "test");
define("DB_CHARSET" , "utf8mb4");
define("DB_USER"    , "root");
define("DB_PASSWORD", "root");                                          //// ""
//include_once './config.php';                                          // Does not work as expected
////require './config.php';


// (E) Instantiate new database object [seq 20230926°1856]
$pdo = new DB();                                                        //// $_DB
//die("DEBUG " . ($pdo->pdao === null) ? 'FALSE' : 'TRUE');
//die("DEBUG " . $pdo->ATTR_SERVER_VERSION);
$pdo->initialize_tables();
