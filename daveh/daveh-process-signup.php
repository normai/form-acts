<?php
// file : 20230926°2051

if (empty($_POST["name"])) {
    die('<p>Name is required — <a href="./daveh-signup.html">Zurück zu signup.html</a>"</p>');
}

if ( ! filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
    die("Valid email is required");
}

if (strlen($_POST["password"]) < 8) {
    die("Password must be at least 8 characters");
}

if ( ! preg_match("/[a-z]/i", $_POST["password"])) {
    die("Password must contain at least one letter");
}

if ( ! preg_match("/[0-9]/", $_POST["password"])) {
    die("Password must contain at least one number");
}

if ($_POST["password"] !== $_POST["password_confirmation"]) {
    die("Passwords must match");
}

$password_hash = password_hash($_POST["password"], PASSWORD_DEFAULT);

$mysqli = require __DIR__ . "/daveh-database.php";

$sql = "INSERT INTO user (name, email, password_hash)
        VALUES (?, ?, ?)";

$stmt = $mysqli->stmt_init();

if ( ! $stmt->prepare($sql)) {
    die("SQL error: " . $mysqli->error);
}

$stmt->bind_param("sss",
                  $_POST["name"],
                  $_POST["email"],
                  $password_hash);

var_dump('[Debug SQL = "' . $sql . '"]');

if ($stmt->execute()) {

    header("Location: daveh-signup-success.html");     // Elegante Möglickeit 'auf der Formular-Seite zu bleiben'
    exit;

} else {

    if ($mysqli->errno === 1062) {                     // Hartcodierte magic 1062 besser als Konstante mit Namen zur Verfügung stellen
        die("Email already taken (or something like)");
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
}
