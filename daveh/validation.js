/**
 * file        : ((20230926°1941 spawned from form-acts/js/validatin.js))
 * origin      :
 */

const validation = new JustValidate("#signup");

validation
    .addField("#name", [
        {
            rule: "required"
        }
    ])
// Sequence provisory shutdown to workaround mysterious errors "Validation: Email already taken." [issue 20230927°1741]
/* */
    .addField("#email", [
        {
            rule: "required"
        },
        {
            rule: "email"
        },
        {
            validator: (value) => () => {
                ////return fetch("./register-validate.php?email=" + encodeURIComponent(value))
                return fetch("./daveh-validate-email.php?email=" + encodeURIComponent(value))
                       .then(function(response) {
                           return response.json();
                       })
                       .then(function(json) {
                           return json.available;
                       });
            },
            errorMessage: "Validation: Email already taken."
        }
    ])
/* */
    .addField("#password", [
        {
            rule: "required"
        },
        {
            rule: "password"
        }
    ])
    .addField("#password_confirmation", [
        {
            validator: (value, fields) => {
                return value === fields["#password"].elem.value;
            },
            errorMessage: "Passwords should match"
        }
    ])
    .onSuccess((event) => {
        document.getElementById("signup").submit();
    });
