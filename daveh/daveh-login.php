<?php
// file : 20230926°2031

$is_invalid = false;

if ($_SERVER["REQUEST_METHOD"] === "POST") {

    $mysqli = require __DIR__ . "/daveh-database.php";

    $sql = sprintf( "SELECT * FROM user WHERE email = '%s'"
                   , $mysqli->real_escape_string($_POST["email"])
                    );

    $result = $mysqli->query($sql);
    $user = $result->fetch_assoc();
    if ($user) {

        if (password_verify($_POST["password"], $user["password_hash"])) {
            session_start();
            session_regenerate_id();
            $_SESSION["user_id"] = $user["id"];
            $_SESSION["user_name"] = $user["name"];
            header("Location: daveh-index.php");
            exit;
        }
    }
    $is_invalid = true;
}

?>
<!DOCTYPE html>
<html>
<head>
    <title>DaveH-Login</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="./../css/cdn.jsdelivr.net_npm_water.css@2_out_water.css">
</head>
<body>

    <h1>DaveH-Login</h1>

    <?php if ($is_invalid): ?>
        <em>Invalid login</em>
    <?php endif; ?>

    <form method="post">
        <label for="email">email</label>
        <input type="email" name="email" id="email"
               value="<?= htmlspecialchars($_POST["email"] ?? "") ?>">

        <label for="password">Password</label>
        <input type="password" name="password" id="password">

        <button>Log in</button>
    </form>

</body>
</html>
