﻿<?php
/**
 * file        : 20230926°1751 controller.php
 * ofilnam     : 2-controller.php
 * after       : https://gist.github.com/code-boxx/372bb08f4763937aed08b678ca900781 [ref 20230923°1423]
 * callers     : toh-view.html
 */

// (A) Provide database connection
require "model.php";                                                    //// "1-model.php"

//
try {
   // (B) Search for users [seq 20230926°1753]
   $searchTerm = isset($_POST["search"]) ? $_POST["search"] : "";

   if (! empty($searchTerm)) {
      $sql = "SELECT * FROM `names` WHERE `Forename` LIKE ?";           // Statement with parameter placeholder
      $rs = $pdo->select($sql, ["%$searchTerm%"]);                      // Run prepared statement with parameter

      // (C) Output result set
      // if (count($rs) > 0) {
           $s = json_encode(count($rs) < 1 ? null : $rs);
           echo($s);
      // }
      // else {
      //    $s = json_encode("<p>No records found.</p>");
      //    echo $s;
      // }

   }
   else {
      echo "<p>Please enter a search term.</p>";
   }
}
catch (Exception $e) {
   // () Handle any exceptions here and optionally log the error [seq 20230926°1755]
   echo("<p>Error: " . $e->getMessage() . "</p>");
}
