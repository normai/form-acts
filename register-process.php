<?php
// file : 20230926°2131 register-process.php
// origin : Spawned from file 20230926°2051 daveh-process-signup.php

// () [seq 20230926°2133]
if (empty($_POST["name"])) {
   die('<p>Name is required — <a href="./register.html">Zurück zu register.html</a>"</p>');
}
if ( ! filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
   die("Valid email is required");
}
if (strlen($_POST["password"]) < 8) {
   die("Password must be at least 8 characters");
}
if ( ! preg_match("/[a-z]/i", $_POST["password"])) {
   die("Password must contain at least one letter");
}
if ( ! preg_match("/[0-9]/", $_POST["password"])) {
   die("Password must contain at least one number");
}
if ($_POST["password"] !== $_POST["password_confirmation"]) {
   die("Passwords must match");
}
$password_hash = password_hash($_POST["password"], PASSWORD_DEFAULT);

/*
// Original sequence using the mysqli-API
$mysqli = require __DIR__ . "/daveh-database.php";
$sql = "INSERT INTO user (name, email, password_hash) VALUES (?, ?, ?)";
$stmt = $mysqli->stmt_init();
if ( ! $stmt->prepare($sql)) {
    die("SQL error: " . $mysqli->error);
}
$stmt->bind_param("sss", $_POST["name"], $_POST["email"], $password_hash);
if ($stmt->execute()) {
    header("Location: daveh-signup-success.html");     // Elegante Möglickeit 'auf der Formular-Seite zu bleiben'
    exit;
} else {
    if ($mysqli->errno === 1062) {                     // Hartcodierte magic 1062 besser als Konstante mit Namen zur Verfügung stellen
        die("Email already taken (or something like)");
    } else {
        die($mysqli->error . " " . $mysqli->errno);
    }
}
*/

// Sequence converted to using PDO [seq 20230927°1731]
// Todo: After it works, the sequence has to be integrated into model.php
// See e.g. https://www.fundaofwebit.com/php-pdo/how-to-insert-data-into-database-using-pdo-in-php [ref 20230926°1722]
require_once "./model.php";                                             // This provides $pdo object
$query = "INSERT INTO users (Name, Email, Pw_Hash) VALUES (:name, :email, :pwhash)";
var_dump($pdo->pdao);
$query_run = $pdo->pdao->prepare($query);
$data = [ ':name'     => $_POST["name"]
         , ':email'   => $_POST["email"]
          , ':pwhash' => $password_hash
           , ];
$query_execute = $query_run->execute($data);
if($query_execute) {
   header("Location: register.html");                                   // Möglickeit 'auf der Formular-Seite zu bleiben'
   exit;
}
else {
   die('Datensatz einfügen gescheitert');                               // Make message more specific
}
