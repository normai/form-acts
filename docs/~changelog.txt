﻿

   version 2023xxxx°xxxx v0.0.x ''
   •
   ⬞

   version 20230930°0951 v0.0.5 'Public'
   • Little fix
   ⬞

   version 20230929°0747 v0.0.4 'Public'
   • Publish project on GitLab
   ⬞

   version 20230928°1721 v0.0.3 'Burgeon'
   • Database 'form-acts' must exist (issue 20230927°2021 'No PDO without database')
   • Create table 'names'
   • Create table 'users'
   ⬞

   version 20230927°1751 v0.0.2 'Sprout'
   • All features shifted from DaveH to Form-Acts
   • Using PDO for login and registering
   • Email validation fails (issue 20230927°1741 'Email vali err')
   ⬞

   version 20230926°2331 v0.0.1 'Seed'
   • Proof-of-concept, not yet feature-ready
   ⬞

   project 20230926°1711 initial collection
   This is a merger from the following precursor projects
   • Project 20230925°1711 php-mvc-dc.v2
   • Project 20230925°1711 php-signup-login.v1
   • Project 20230921°1701 gemüse3
   • Newly written glue code
   ⬞

   ———————————————————————
   [file 20230926°1723] ⬞Ω
