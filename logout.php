<?php
/**
 * file        : 20230926°1841
 * origin      : daveh-logout.php
 */

session_start();

session_destroy();

header("Location: login.html");
exit;
